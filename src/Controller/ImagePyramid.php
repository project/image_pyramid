<?php

namespace Drupal\image_pyramid\Controller;

use Drupal\Core\Controller\ControllerBase;

class ImagePyramid extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   */
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => $this->t('Hello, World!'),
      );
  }

}